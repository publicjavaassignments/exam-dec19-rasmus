package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    TextField txtFieldOne, txtFieldTwo, txtFieldThree, txtFieldFour;
    @FXML
    Label lblAccess;

    public void checkAccess() {
        String correctPin = "5673";
        String enteredPin = txtFieldOne.getText() + txtFieldTwo.getText() + txtFieldThree.getText() + txtFieldFour.getText();

        if(enteredPin.equals(correctPin)) {
            lblAccess.setText("Okay");
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Acces not allowed. Please try again");
            alert.showAndWait();
        }
    }


}
