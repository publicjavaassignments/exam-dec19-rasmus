package Opgave4;

import java.util.ArrayList;
import java.util.Scanner;

public class CalculateInput2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Double> numberList = new ArrayList<>();
        double input;
        int amountOfNumbers = 0;

        System.out.println("Please enter some integers (type -1 to stop):");
        while(true) {
            input = scanner.nextDouble();
            if(input == -1 && amountOfNumbers >= 3) {
                break;
            } else if (input == -1 && amountOfNumbers < 3) {
                System.out.println("Please enter atleast 3 numbers.");
            } else {
                numberList.add(input);
            }
            amountOfNumbers++;
        }

        double sum = getSum(numberList);
        System.out.printf("Sum: %1.2f", sum);

    }

    public static double getSum(ArrayList<Double> numberList) {
        double sum = 0;
        double smallest = 0;
        double largest = 0;

        for (double number : numberList) {
            System.out.println(number);
            if(smallest >= number) {
                smallest = number;
            } else if (largest <= number){
                largest = number;
            } else {
                sum = sum + number;
            }
        }
        return sum;
    }
}
