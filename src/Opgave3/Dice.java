package Opgave3;

import java.util.Random;

public class Dice {

    public static void main(String[] args) {

        int correctNumber = 0;
        int counter = 0;
        do {
            correctNumber = rollDice();
            counter++;
        } while (correctNumber != 3);

        System.out.println("It took " + counter + " to generate the number 3.");

        System.out.println(rollDice100());

    }

    public static int rollDice() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(6) + 1;
        return randomNumber;
    }

    public static int rollDice100() {
        int average = 0;
        int randomNumber = 0;
        int rollAmount = 100;

        for(int i = 0; i < rollAmount; i++) {
            randomNumber = rollDice();
            average = average + randomNumber;
        }

        return average/rollAmount;
    }




}
