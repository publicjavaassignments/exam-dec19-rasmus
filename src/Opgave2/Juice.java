package Opgave2;

public class Juice implements Product{
    private double price;
    private String sortOfFruit;
    public Juice(double price, String sortOfFruit)
    {
        this.price = price;
        this.sortOfFruit = sortOfFruit;
    }

    @Override
    public double price() {
        return this.price;
    }

    @Override
    public String description() {
        return this.sortOfFruit;
    }

    @Override
    public String toString() {
        return "Description: " + description() + "\nPrice: " + price();
    }
}
