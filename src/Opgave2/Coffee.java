package Opgave2;

public class Coffee implements Product{
    private double price;
    private String brand;
    private String origin;
    public Coffee(double price, String brand, String origin)
    {
        this.price = price;
        this.brand = brand;
        this.origin = origin;
    }

    @Override
    public double price() {
        return this.price;
    }

    @Override
    public String description() {
        return this.brand;
    }

    @Override
    public String toString() {
        return "Description: " + description() + "\nPrice: " + price();
    }
}
