package Opgave2;

import java.util.ArrayList;

public class TestClass {

    public static void main(String[] args) {

        ArrayList<Product> products = new ArrayList<>();

        Juice orange = new Juice(25, "Orange");
        Juice apple = new Juice(20, "Apple");
        Juice strawberry = new Juice(30, "Strawberry");
        products.add(orange);
        products.add(apple);
        products.add(strawberry);

        Coffee black = new Coffee(20, "Starbucks", "Africa");
        Coffee espresso = new Coffee(25, "Starbucks", "Italy");
        Coffee latte = new Coffee(10, "Home made", "Italy");
        products.add(black);
        products.add(espresso);
        products.add(latte);

        for (Product product: products) {
            System.out.println(product.description());
            System.out.println(product.price());
        }

        for(Product product : products) {
            System.out.println(product.toString());
        }

    }

}
