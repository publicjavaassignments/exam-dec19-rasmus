package Opgave2;

public interface Product {
    double price();
    String description();
}
