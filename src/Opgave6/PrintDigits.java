package Opgave6;

public class PrintDigits {

    public static void main(String[] args) {
        printDigitsOnRowLoop(5);
        System.out.println();
        printDigitOnRowRecursive(7);
    }

    public static void printDigitsOnRowLoop(int digit) {
        int printer = 0;
        for(int i = 0; i <= digit; i++) {
            for(int j = 0; j < 10; j++) {
                System.out.print(printer);
            }
            System.out.println();
            printer++;
        }
    }

    public static int printDigitOnRowRecursive(int digit) {
        if(digit < 0) {
            return 0;
        }
        System.out.print("" + digit + digit + digit + digit + digit + digit + digit + digit + digit + digit);
        System.out.println();
        return printDigitOnRowRecursive(digit-1);
    }

}
